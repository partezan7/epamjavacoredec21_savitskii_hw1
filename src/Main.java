import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Enter X");
        float x = scanner.nextFloat();

        System.out.println("Enter Y");
        float y = scanner.nextFloat();

        System.out.println("You entered: X=" + x + ", Y=" + y);

        int signX = determineSign(x);
        int signY = determineSign(y);

        if (signX == 0 && signY == 0) {
            System.out.println("Point of zero");
        } else {
            switch (signX) {
                case 0:
                    System.out.println("Axis Y");
                    break;
                case 1:
                    switch (signY) {
                        case 0:
                            System.out.println("Axis X");
                            break;
                        case 1:
                            System.out.println("I");
                            break;
                        case -1:
                            System.out.println("IV");
                            break;
                    }
                    break;
                case -1:
                    switch (signY) {
                        case 0:
                            System.out.println("Axis X");
                            break;
                        case 1:
                            System.out.println("II");
                            break;
                        case -1:
                            System.out.println("III");
                            break;
                    }
                    break;
            }
        }
    }

    private static int determineSign(float f) {
        if (f == 0) return 0;
        if (f > 0) {
            return 1;
        } else return -1;
    }
}
